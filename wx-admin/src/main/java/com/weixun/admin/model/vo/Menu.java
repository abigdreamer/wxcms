package com.weixun.admin.model.vo;

import java.util.List;

public class Menu {
    private Integer id;             //菜单主键
    private String name;        //'模块名称'
    private String menu_icon;        //'图标'
    private String menu_parent;         //'父菜单ID'
    private String menu_url;            //url地址
    private Integer menu_number;         //排序值
    private String menu_type;         //类型值0菜单，1资源
    //不在数据库中的字段
    private List<Menu> children;        //子菜单

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMenu_icon() {
        return menu_icon;
    }

    public void setMenu_icon(String menu_icon) {
        this.menu_icon = menu_icon;
    }

    public String getMenu_parent() {
        return menu_parent;
    }

    public void setMenu_parent(String menu_parent) {
        this.menu_parent = menu_parent;
    }

    public String getMenu_url() {
        return menu_url;
    }

    public void setMenu_url(String menu_url) {
        this.menu_url = menu_url;
    }

    public Integer getMenu_number() {
        return menu_number;
    }

    public void setMenu_number(Integer menu_number) {
        this.menu_number = menu_number;
    }

    public String getMenu_type() {
        return menu_type;
    }

    public void setMenu_type(String menu_type) {
        this.menu_type = menu_type;
    }

    public List<Menu> getChildren() {
        return children;
    }

    public void setChildren(List<Menu> children) {
        this.children = children;
    }
}
